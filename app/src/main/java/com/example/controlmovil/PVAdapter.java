package com.example.controlmovil;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class PVAdapter extends RecyclerView.Adapter<PVAdapter.ViewHolder> {

    private Context context;
    private List<PuntoVenta> list;

    public PVAdapter(Context context, List<PuntoVenta> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        PuntoVenta puntoVenta = list.get(position);
        holder.textTitle.setText(puntoVenta.getNombre());
        holder.textFolio.setText(puntoVenta.getFolio());
        holder.textCliente.setText("Aún no es cliente.");
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textTitle, textCliente, textFolio;
        public ViewHolder (View itemView){
            super(itemView);
            textTitle = itemView.findViewById(R.id.pv_title);
            textCliente = itemView.findViewById(R.id.pv_cliente);
            textFolio = itemView.findViewById(R.id.pv_id_oculto);
        }
    }
}
