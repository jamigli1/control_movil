package com.example.controlmovil;

public class ClienteC {

    private String nombre, tipo, satcli, dis;

    public ClienteC() {
    }

    public ClienteC(String nombre, String tipo, String satcli, String dis) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.satcli = satcli;
        this.dis = dis;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSatcli() {
        return satcli;
    }

    public void setSatcli(String satcli) {
        this.satcli = satcli;
    }


    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }
}
