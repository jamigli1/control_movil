package com.example.controlmovil;

import android.Manifest;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.controlmovil.Entidades.Prospecto.View.Prospectos;
import com.example.controlmovil.LocationTracker.LocationRequestHelper;
import com.example.controlmovil.LocationTracker.LocationResultHelper;
import com.example.controlmovil.LocationTracker.LocationUpdatesBroadcastReceiver;
import com.example.controlmovil.NetworkL.NetworkStateReceiver;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class MainMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, NetworkStateReceiver.NetworkStateReceiverListener , GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String prefencesN = "userdata";

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    //1 Minuto
    private static final long UPDATE_INTERVAL = 60 * 1000;
    //Dentro de la aplicación de 30 a 40 segundos
    private static final long FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL / 2;
    //Fuera de la app en segundo plano
    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL/2;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    //Network
    private NetworkStateReceiver networkStateReceiver;

    private TextView username;
    private View header;
    private NavigationView navigationView;
    private Double lat, lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        obtenerLction();

        SharedPreferences settings = this.getSharedPreferences(prefencesN, this.MODE_PRIVATE);
        String user = settings.getString("usuario", "-1");

        header = ((NavigationView) findViewById(R.id.nav_view)).getHeaderView(0);
        username = (TextView) header.findViewById(R.id.main_user);

        username.setText(user.toUpperCase());

        navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        if (!checkPermissions()) {
            requestPermissions();
        }

        buildGoogleApiClient();

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    //Verifica el tipo de conexión

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean checkConnectionType(){
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isWifiConn = false;

        for (Network network : connManager.getAllNetworks()){
            NetworkInfo networkInfo = connManager.getNetworkInfo(network);
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI){
                isWifiConn = true;
            }
        }

        return isWifiConn;
    }

    //Estados

    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onStop() {
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    //Termina estados

    //Empieza Location

    private void createLocationRequest(){
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);

        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
    }

    private void buildGoogleApiClient(){
        if (mGoogleApiClient != null){
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this,this)
                .addApi(LocationServices.API)
                .build();

        createLocationRequest();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle){
        Log.i("GPSGPS","GoogleApiCliente Connected");
    }

    private PendingIntent getPendingIntent(){
        Intent intent = new Intent(this, LocationUpdatesBroadcastReceiver.class);
        intent.setAction(LocationUpdatesBroadcastReceiver.ACTION_PROCESS_UPDATES);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onConnectionSuspended(int i){
        final String text = "Connection suspended";
        Log.w("GPSGPS",text + ": error code " + i);
        Toast.makeText(this, "Connection suspended",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        final String text = "Exception while connecting to Google Play services";
        Log.w("GPSGPS", text + ": " + connectionResult.getErrorMessage());
        Toast.makeText(this, text,Toast.LENGTH_SHORT).show();
    }

    //Termina Location Service

    //Checa permisos
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i("GPSGPS", "Displaying permission rationale to provide additional context.");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(MainMenu.this,new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
                }
            });

            builder.setNegativeButton(R.string.btn3, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Log.i("GPSGPS", "Requesting permission");
            ActivityCompat.requestPermissions(MainMenu.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i("GPSGPS", "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.i("GPSGPS", "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package",
                                BuildConfig.APPLICATION_ID, null);
                        intent.setData(uri);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                builder.setNegativeButton(R.string.btn3, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals(LocationResultHelper.KEY_LOCATION_UPDATES_RESULT)) {
            //mLocationUpdatesResultView.setText(LocationResultHelper.getSavedLocationResult(this));
            //Log.d("Dentro de la app", LocationResultHelper.getSavedLocationResult(this));
        } else if (s.equals(LocationRequestHelper.KEY_LOCATION_UPDATES_REQUESTED)) {
            //updateButtonsState(LocationRequestHelper.getRequesting(this));
        }
    }

    public void requestLocationUpdates() {
        try {
            Log.i("GPSGPS", "Starting location updates");
            LocationRequestHelper.setRequesting(this, true);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, getPendingIntent());
        } catch (SecurityException e) {
            LocationRequestHelper.setRequesting(this, false);
            e.printStackTrace();
        }
    }

    public void removeLocationUpdates() {
        Log.i("GPSGPS", "Removing location updates");
        LocationRequestHelper.setRequesting(this, false);
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                getPendingIntent());
    }

    // <------------------- Termina permisos ------------------->

    public void obtenerLction(){
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number can be used
            return;
        }

        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                lat = location.getLatitude();
                lon = location.getLongitude();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {}

            @Override
            public void onProviderEnabled(String provider) {}

            @Override
            public void onProviderDisabled(String provider) {}
        };

        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,1,0,locationListener);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent i;

        if (id == R.id.nav_prospectos) {
            i = new Intent(this, Prospectos.class);
            startActivity(i);
        } else if (id == R.id.nav_pVenta) {
            i = new Intent(this,PVentasActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_visitas) {
            i = new Intent(this,VisitasActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_ventas) {
            i = new Intent(this,VentasActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_vPendientes) {
            i = new Intent(this,VentasPendientesActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_depositos) {
            Intent intent1 = new Intent(this,CapturaDepActivity.class);
            Intent intent2 = new Intent(this,ListarDepActivity.class);
            selectorMenu("Capturar depósito", "Listar mis depositos", intent1, intent2);
        } else if (id == R.id.nav_microp){
            Intent intent1 = new Intent(this, NewClienteActivity.class);
            Intent intent2 = new Intent(this,ListarClientesActivity.class);
            selectorMenu("Crear cliente", "Listar mis solicitudes", intent1, intent2);
        } else if (id == R.id.nav_comisiones){
            i = new Intent(this,ComisionesActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_about){

        } else if (id == R.id.nav_logout){
            logout();
        } else if (id == R.id.nav_iniciarjornada){
            if (item.getTitle().toString().toLowerCase().contains("iniciar")) {
                item.setTitle(R.string.terminar_jornada);
                requestLocationUpdates();
            } else {
                item.setTitle(R.string.iniciar_jornada);
                removeLocationUpdates();
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onClickBtnPrspctos(View v){
        Intent i;
        i = new Intent(this,Prospectos.class);
        startActivity(i);
    }

    public void onClickPventas(View v){
        Intent i = new Intent(this,PVentasActivity.class);
        startActivity(i);
    }

    public void onClickVisitas(View v){
        Intent i = new Intent(this,VisitasActivity.class);
        startActivity(i);
    }

    public void onClickVentas(View v){
        Intent i;
        i = new Intent(this,VentasActivity.class);
        startActivity(i);
    }

    public void onClickVentasP(View v){
        Intent i = new Intent(this,VentasPendientesActivity.class);
        startActivity(i);
    }

    public void onClickDepositos(View v){
        Intent intent1 = new Intent(this,CapturaDepActivity.class);
        Intent intent2 = new Intent(this,ListarDepActivity.class);
        selectorMenu("Capturar depósito", "Listar mis depositos", intent1, intent2);
    }

    public void onClickMicro(View v){
        Intent intent1 = new Intent(this, NewClienteActivity.class);
        Intent intent2 = new Intent(this,ListarClientesActivity.class);
        selectorMenu("Crear cliente", "Listar mis solicitudes", intent1, intent2);
    }

    public void onClickComisiones(View v){
        Intent i = new Intent(this,ComisionesActivity.class);
        startActivity(i);
    }

    public void selectorMenu(String opc1, String opc2, final Intent intent1, final Intent intent2){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);

        btnopc1.setText(opc1);
        btnopc2.setText(opc2);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent1);
                dialog.dismiss();
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent2);
                dialog.dismiss();
            }
        });

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //Metodos network receiver

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void networkAvailable(){
        checkConnectionType();
        Log.d("CONNECTION CHANGED", "conección reestablecida");
    }

    @Override
    public void networkUnavailable(){
        Log.d("CONNECTION CHANGED","conección perdida");
    }

    private void logout(){
        Intent i = new Intent(this,MainActivity.class);
        SharedPreferences settings = this.getSharedPreferences(prefencesN,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.apply();
        startActivity(i);
        this.finish();
    }

}
