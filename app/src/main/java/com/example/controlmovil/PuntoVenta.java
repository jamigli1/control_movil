package com.example.controlmovil;

public class PuntoVenta {

    private String id;
    private String nombre;
    private String coordenadas;
    private String codigo;
    private String estado;
    private String tipo;
    private String subtipo;
    private String titularid;
    private String fecha;
    private String total;
    private String folio;


    public PuntoVenta(){

    }

    public PuntoVenta(String id, String nombre, String coordenadas, String codigo, String estado, String tipo, String subtipo, String titularid, String fecha, String total, String folio) {
        this.id = id;
        this.nombre = nombre;
        this.coordenadas = coordenadas;
        this.codigo = codigo;
        this.estado = estado;
        this.tipo = tipo;
        this.subtipo = subtipo;
        this.titularid = titularid;
        this.fecha = fecha;
        this.total = total;
        this.folio = folio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSubtipo() {
        return subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    public String getTitularid() {
        return titularid;
    }

    public void setTitularid(String titularid) {
        this.titularid = titularid;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }
}
