package com.example.controlmovil;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class DepositoAdapter extends RecyclerView.Adapter<DepositoAdapter.ViewHolder> {
    private Context context;
    private List<DepositoA> list;

    public DepositoAdapter(Context context, List<DepositoA> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public DepositoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_deposito,parent,false);
        return new DepositoAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DepositoAdapter.ViewHolder holder, int position){
        DepositoA depositoA = list.get(position);
        holder.txtFecha.setText(depositoA.getFechaR());
        holder.txtMonto.setText("$ ".concat(depositoA.getMonto()));
        holder.txtDecripcion.setText(depositoA.getBanco().concat(": ").concat(depositoA.getMovimiento()));
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txtFecha, txtMonto, txtDecripcion;

        public ViewHolder(View itemView){
            super(itemView);
            txtFecha = itemView.findViewById(R.id.deposito_fecha);
            txtMonto = itemView.findViewById(R.id.deposito_monto);
            txtDecripcion = itemView.findViewById(R.id.deposito_movimiento);
        }

    }
}
