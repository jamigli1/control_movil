package com.example.controlmovil;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

public class NewClienteActivity extends AppCompatActivity {

    private Spinner estadosr7, porcentaje;
    private AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_cliente);

        estadosr7 = (Spinner) findViewById(R.id.estador7_spinner);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.estados_r7_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        estadosr7.setAdapter(arrayAdapter);

        porcentaje = (Spinner) findViewById(R.id.porcentaje_spinner);
        ArrayAdapter<CharSequence> arrayAdapter2 = ArrayAdapter.createFromResource(this,R.array.porcentaje_array, android.R.layout.simple_spinner_item);
        arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        porcentaje.setAdapter(arrayAdapter2);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        reglasValidacion();
    }

    public void onClickEnviarSolicitud(View v){
        nonEmpty((EditText) findViewById(R.id.newCliente_nombreNegocio));
        nonEmpty((EditText) findViewById(R.id.newCliente_Colonia));
        nonEmpty((EditText) findViewById(R.id.newCliente_calle));
        nonEmpty((EditText) findViewById(R.id.newCliente_exterior));
        nonEmpty((EditText) findViewById(R.id.newCliente_RFC));

        awesomeValidation.clear();
        if (awesomeValidation.validate() && nonEmpty((EditText) findViewById(R.id.newCliente_nombreNegocio))
                && nonEmpty((EditText) findViewById(R.id.newCliente_Colonia)) && nonEmpty((EditText) findViewById(R.id.newCliente_calle))
                && nonEmpty((EditText) findViewById(R.id.newCliente_exterior)) && nonEmpty((EditText) findViewById(R.id.newCliente_RFC))){
            Log.d("Validacion","COMPLETAAAAAAAAAAAAAAAAAAAA");
        }
    }

    public void reglasValidacion(){
        String regexName = "^[a-zA-Z]+(([',. -ñÑ][a-zA-Z ])?[a-zA-Z]*)*$";
        String requerido = "[a-zA-Z\\s]+";
        awesomeValidation.addValidation(this,R.id.newCliente_nombre,regexName,R.string.errorNombre);
        awesomeValidation.addValidation(this,R.id.newCliente_apellidoP,regexName,R.string.errorNombre);
        awesomeValidation.addValidation(this,R.id.newCliente_apellidoM,regexName,R.string.errorNombre);
        awesomeValidation.addValidation(this,R.id.newCliente_celular, "^[+]?[0-9]{10}$",R.string.errorTelef);
        awesomeValidation.addValidation(this,R.id.newCliente_email,Patterns.EMAIL_ADDRESS,R.string.errorNombre);
        awesomeValidation.addValidation(this,R.id.newCliente_medioVenta, requerido,R.string.errorVacio);
        awesomeValidation.addValidation(this,R.id.newCliente_municipio,regexName,R.string.errorVacio);
        awesomeValidation.addValidation(this,R.id.newCliente_copostal,"^[0-9]{5}$",R.string.errorVacio);
        awesomeValidation.addValidation(this,R.id.newCliente_ciudad,regexName,R.string.errorVacio);
    }

    public boolean nonEmpty(EditText editText){
        if (!TextUtils.isEmpty(editText.getText())){
            return true;
        } else {
            editText.setError("Error: Este campo es requerido.");
            return false;
        }
    }

}