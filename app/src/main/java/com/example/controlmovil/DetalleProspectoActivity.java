package com.example.controlmovil;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DetalleProspectoActivity extends AppCompatActivity implements OnMapReadyCallback {

    EditText nombre, titular, telefonoT, email;
    EditText colonia, copostal, calle, municipioT, ciudadT;
    Spinner estado;

    private double lat, lon;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_prospecto);

        nombre = (EditText) findViewById(R.id.detalle_nombre_pv);
        titular = (EditText) findViewById(R.id.detalle_nombre_titular);
        telefonoT = (EditText) findViewById(R.id.detalle_telefono);
        email = (EditText) findViewById(R.id.detalle_email);
        colonia = (EditText) findViewById(R.id.detalle_colonia);
        copostal = (EditText) findViewById(R.id.detalle_cp);
        calle = (EditText) findViewById(R.id.detalle_calle);
        municipioT = (EditText) findViewById(R.id.detalle_municipio);
        ciudadT = (EditText) findViewById(R.id.detalle_ciudad);

        estado = (Spinner) findViewById(R.id.detalle_estado);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.estados_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        estado.setAdapter(arrayAdapter);

        Bundle extras = getIntent().getExtras();
        getData(extras.getString("folio"));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapdetalle);
        mapFragment.getMapAsync(this);

    }

    private void getData(final String folio){

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/PVentas.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d("Response",response.toString());
                try{
                    String cad = response.substring(1,response.length());
                    JSONObject object = new JSONObject(cad);
                    putData(object);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method","verPV");
                params.put("idpv",folio);
                return params;
            }
        };
        requestQueue.add(stringRequest);

    }

    public void putData(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String nombrepv = jsonArray.getString(0).replace("[","").replace("]","");
        String nombret = jsonArray.getString(1).replace("[","").replace("]","");
        String telefono = jsonArray.getString(2).replace("[","").replace("]","");
        String estadot = jsonArray.getString(3).replace("[","").replace("]","");
        String municipio = jsonArray.getString(4).replace("[","").replace("]","");
        String ciudad = jsonArray.getString(5).replace("[","").replace("]","");
        String colonia = jsonArray.getString(6).replace("[","").replace("]","");
        String codpostal = jsonArray.getString(7).replace("[","").replace("]","");
        String calle = jsonArray.getString(8).replace("[","").replace("]","");
        /*String tipo = jsonArray.getString(9).replace("[","").replace("]","");
        String idt = jsonArray.getString(10).replace("[","").replace("]","");
        String idpv = jsonArray.getString(11).replace("[","").replace("]","");
        String coord = jsonArray.getString(12).replace("[","").replace("]","");
        String coddis = jsonArray.getString(13).replace("[","").replace("]","");*/
        String lati = jsonArray.getString(14).replace("[","").replace("]","");
        String longi = jsonArray.getString(15).replace("[","").replace("]","");

        notNull(nombre,nombrepv);
        notNull(titular,nombret);
        notNull(telefonoT,telefono);
        notNull(municipioT,municipio);
        notNull(this.colonia,colonia);
        notNull(ciudadT,ciudad);
        notNull(this.copostal,codpostal);
        notNull(this.calle,calle);

        if (!estadot.contains("null")){
            ArrayAdapter arrayAdapter = (ArrayAdapter) estado.getAdapter();
            int position = arrayAdapter.getPosition(estadot);
            estado.setSelection(position);
        }

        if (!longi.isEmpty() && !longi.contains("null")) {

            lon = Double.parseDouble(longi);
            lat = Double.parseDouble(lati);

            LatLng sydney = new LatLng(lat, lon);
            mMap.addMarker(new MarkerOptions().position(sydney).title(nombrepv));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

        } else {
            LatLng def = new LatLng(19.4978,-99.1269);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(def));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(0.0f));
        }
    }

    public void notNull(EditText editText, String cadena){

        if (!cadena.contains("null")){
            editText.setText(cadena);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
    }
}
