package com.example.controlmovil;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DetalleVentaActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<PuntoVenta> ventasLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_venta);

        mList = (RecyclerView) findViewById(R.id.rView_DetVenta);
        ventasLista = new ArrayList<>();
        mAdapter = new DetalleVAdapter(getApplicationContext(),ventasLista);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        Bundle extras = getIntent().getExtras();
        listarDetalleVenta(extras.getString("folio"));
        //Log.d("TIPO TIENDA", extras.getString("folio"));

    }

    private void listarDetalleVenta(final String folio) {
        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d("Response",response.toString());
                try{
                    String cad = response.substring(1,response.length());
                    JSONObject object = new JSONObject(cad);

                    if(object.length() != 0){
                        generaListV(object);
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method","detalleVenta");
                params.put("folio",folio);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    public void generaListV(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String codigo = jsonArray.getString(3).replace("[","").replace("]","");
        String descripcion = jsonArray.getString(4).replace("[","").replace("]","");
        String monto = jsonArray.getString(5).replace("[","").replace("]","");

        PuntoVenta puntoVenta = new PuntoVenta();
        puntoVenta.setNombre(descripcion.replace("\"",""));
        puntoVenta.setCodigo(codigo.replace("\"",""));
        puntoVenta.setTotal(monto.replace("\"",""));
        ventasLista.add(puntoVenta);
    }
}
