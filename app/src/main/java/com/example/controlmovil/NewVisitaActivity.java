package com.example.controlmovil;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SimpleTimeZone;

public class NewVisitaActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<PuntoVenta> list;
    private double lat, lon;
    private AlertDialog.Builder alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_visita);

        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number can be used
            return;
        }

        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                lat = location.getLatitude();
                lon = location.getLongitude();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        };

        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, locationListener);

        mList = (RecyclerView) findViewById(R.id.rView_NewVisita);
        list = new ArrayList<>();
        mAdapter = new NewVisitaAdapter(getApplicationContext(), list);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("CONTROL MOVIL");
        alertDialog.setMessage("¿Está seguro que desea registrar una visita?");

        final Intent i = new Intent(this, VisitasActivity.class);


        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final String title1 = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.nvisita_idpv)).getText().toString();
                checkLction(lat,lon);
                selectorMenu("SI", title1, i);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        listarPtoVentas();

    }

    private void checkLction(Double lat, Double lon) {
        if (lat == 0) {
            try {
                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
                }
                Location lm = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                this.lat = lm.getLatitude();
                this.lon = lm.getLongitude();
            } catch (Exception e) {
                Log.e("Error",e.getMessage());
            }
        }
    }

    private void registrarVisita(final String idpv) {
        SharedPreferences settings = this.getSharedPreferences(prefencesN, this.MODE_PRIVATE);
        final String uid = settings.getString("id", "-1");


        if (!uid.equals("-1")) {

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VisitasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Response",response.toString());
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = simpleDateFormat.format(c.getTime());

                    params.put("method", "registrarVisita");
                    params.put("fecha", formattedDate);
                    params.put("latitud",String.valueOf(lat));
                    params.put("longitud",String.valueOf(lon));
                    params.put("descripcion","Se registro una visita");
                    params.put("user_id", uid);
                    params.put("pv",idpv);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    private void listarPtoVentas() {

        SharedPreferences settings = this.getSharedPreferences(prefencesN, this.MODE_PRIVATE);
        final String uid = settings.getString("id", "-1");
        if (!uid.equals("-1")) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/PVentas.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("Response",response.toString());
                    try {
                        String cad = response.substring(1, response.length());
                        JSONObject object = new JSONObject(cad);
                        if (object.getString("usuario_id").contains(uid)) {
                            generaList(object);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","listar");
                    params.put("uid",uid);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaList(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String codigo = jsonArray.getString(3).replace("[","").replace("]","");
        String nombre = jsonArray.getString(1).replace("[","").replace("]","");
        String idpv = jsonArray.getString(0).replace("[","").replace("]","");
        String[] cod_part = codigo.split(",");
        String[] arr_nom = nombre.split(",");
        String[] arr_idpv = idpv.split(",");
        List<Integer> indices = new ArrayList<Integer>();

        for (int i=0; i<cod_part.length; i++){
            if (!cod_part[i].toString().contains("null")) {
                indices.add(i);
            }
        }

        for (int i=0; i<indices.size(); i++){
            PuntoVenta puntoVenta = new PuntoVenta();
            puntoVenta.setNombre(arr_nom[indices.get(i)].replace("\"",""));
            puntoVenta.setCodigo(cod_part[indices.get(i)].replace("\"",""));
            puntoVenta.setId(arr_idpv[indices.get(i)].replace("\"",""));
            list.add(puntoVenta);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1001: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                    //Start your service here
                }
            }
        }
    }

    public void selectorMenu(String opc1, final String title1, final Intent i){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);
        TextView header = (TextView) mView.findViewById(R.id.header);

        btnopc1.setText(opc1);
        btnopc2.setText("Cancelar");
        header.setText("¿Desea registrar una visita?");

        final Handler handler = new Handler();
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.nw_visit_progrssbar);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (lat == 0) {
                    progressBar.setVisibility(View.VISIBLE);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            registrarVisita(title1);
                            finish();
                            startActivity(i);
                            //Log.d("Last",String.valueOf(lat));
                        }
                    }, 18000);
                } else {
                    registrarVisita(title1);
                    finish();
                    startActivity(i);
                    //Log.d("Reciente",String.valueOf(lat));
                }
                //registrarVisita(title1);
                //startActivity(i);
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btncancel.setVisibility(View.GONE);

        dialog.show();
    }
}
