package com.example.controlmovil;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class NewVisitaAdapter extends RecyclerView.Adapter<NewVisitaAdapter.ViewHolder> {

    private Context context;
    private List<PuntoVenta> list;

    public NewVisitaAdapter(ArrayList<PuntoVenta> pventas){
        this.list = pventas;
    }

    public NewVisitaAdapter(Context context, List<PuntoVenta> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public NewVisitaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_nvisita, parent, false);
        return new NewVisitaAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NewVisitaAdapter.ViewHolder holder, int position){
        PuntoVenta puntoVenta = list.get(position);
        holder.textTitle.setText(puntoVenta.getNombre());
        holder.textCliente.setText(puntoVenta.getCodigo());
        holder.txtIdPv.setText(puntoVenta.getId());
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textTitle, textCliente, txtIdPv;
        public ViewHolder (View itemView){
            super(itemView);
            textTitle = itemView.findViewById(R.id.nvisita_nombe);
            textCliente = itemView.findViewById(R.id.nvisita_codigo);
            txtIdPv = itemView.findViewById(R.id.nvisita_idpv);
        }
    }
}
