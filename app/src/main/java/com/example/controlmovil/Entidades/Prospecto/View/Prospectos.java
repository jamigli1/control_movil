package com.example.controlmovil.Entidades.Prospecto.View;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.controlmovil.DetalleProspectoActivity;
import com.example.controlmovil.MapaActivity;
import com.example.controlmovil.NewProspectoActivity;
import com.example.controlmovil.PVAdapter;
import com.example.controlmovil.PuntoVenta;
import com.example.controlmovil.R;
import com.example.controlmovil.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Prospectos extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<PuntoVenta> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prospectos);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mList = (RecyclerView) findViewById(R.id.rView_Prosp);
        list = new ArrayList<>();
        mAdapter = new PVAdapter(getApplicationContext(),list);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        final Intent i = new Intent(this, DetalleProspectoActivity.class);

        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final String title1 = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pv_id_oculto)).getText().toString();
                //Log.d("FOLIOOOOO",title1);
                i.putExtra("folio",title1);
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        listarPtoVentas();
        final Intent i2 = new Intent(this,MapaActivity.class);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Selector("COMPETENCIA","MICROTEC",i2);
            }
        });
    }

    private void listarPtoVentas(){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String uid = settings.getString("id","-1");
        if(!uid.equals("-1")){
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/PVentas.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("Response",response.toString());
                    try{
                        String cad = response.substring(1,response.length());
                        JSONObject object = new JSONObject(cad);
                        if(object.getString("usuario_id").contains(uid)){
                            generaList(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","listar");
                    params.put("uid",uid);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }

    }


    public void generaList(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String codigo = jsonArray.getString(3).replace("[","").replace("]","");
        String nombre = jsonArray.getString(1).replace("[","").replace("]","");
        String folio = jsonArray.getString(0).replace("[","").replace("]","");
        String[] cod_part = codigo.split(",");
        String[] arr_nom = nombre.split(",");
        String[] arr_folio = folio.split(",");
        List<Integer> indices = new ArrayList<Integer>();

        for (int i=0; i<cod_part.length; i++){
            if (cod_part[i].toString().contains("null")) {
                indices.add(i);
            }
        }

        for (int i=0; i<indices.size(); i++){
            PuntoVenta puntoVenta = new PuntoVenta();
            puntoVenta.setNombre(arr_nom[indices.get(i)].replace("\"",""));
            puntoVenta.setFolio(arr_folio[indices.get(i)].replace("\"",""));
            list.add(puntoVenta);
        }
    }

    public void Selector(String opc1, String opc2, final Intent intent1){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);
        TextView header = (TextView) mView.findViewById(R.id.header);

        btnopc1.setText(opc1);
        btnopc2.setText(opc2);
        header.setText("CREAR NUEVO PROSPECTO");

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent1.putExtra("tipotienda","comp");
                startActivity(intent1);
                dialog.dismiss();
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent1.putExtra("tipotienda","micro");
                startActivity(intent1);
                dialog.dismiss();
            }
        });

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

}
