package com.example.controlmovil;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ClientesAdapter extends RecyclerView.Adapter<ClientesAdapter.ViewHolder> {

    private Context context;
    private List<ClienteC> list;

    public ClientesAdapter(Context context, List<ClienteC> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ClientesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_pventa,parent,false);
        return new ClientesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ClientesAdapter.ViewHolder holder, int position){
        ClienteC clienteC = list.get(position);
        holder.textCliente.setText(clienteC.getNombre());
        holder.textTipo.setText(clienteC.getDis());
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textCliente, textTipo;

        public ViewHolder(View itemView){
            super(itemView);
            textCliente = itemView.findViewById(R.id.pventa_nombre);
            textTipo = itemView.findViewById(R.id.pventa_tipo);
        }

    }

}
