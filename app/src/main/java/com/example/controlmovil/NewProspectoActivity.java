package com.example.controlmovil;

import android.app.Activity;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class NewProspectoActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int PICK_IMAGE = 1;

    public LinearLayout form1, form2, form3, form4, form5;
    public LinearLayout form6, form7, form8, form9, form10, form11;
    public LinearLayout imagePicker;
    public double lat, lon;
    public String tipot;
    Button mostrarL;

    private Spinner estado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_prospecto);

        form1 = (LinearLayout) findViewById(R.id.lnl_calf_ubicacion);
        form2 = (LinearLayout) findViewById(R.id.lnl_datos_cliente);
        form3 = (LinearLayout) findViewById(R.id.lnl_datos_establecimiento);
        form4 = (LinearLayout) findViewById(R.id.lnl_inmobiliario);
        form5 = (LinearLayout) findViewById(R.id.lnl_letreros_lum);
        form6 = (LinearLayout) findViewById(R.id.lnl_mobiliario);
        form7 = (LinearLayout) findViewById(R.id.lnl_pre_establecimiento);
        form8 = (LinearLayout) findViewById(R.id.lnl_proveedor);
        form9 = (LinearLayout) findViewById(R.id.lnl_publicidad);
        form10 = (LinearLayout) findViewById(R.id.lnl_ubicacion);
        form11 = (LinearLayout) findViewById(R.id.lnl_venta);
        mostrarL = (Button) findViewById(R.id.btn_mostrar);
        imagePicker = (LinearLayout) findViewById(R.id.form_imagepicker);

        estado = (Spinner) findViewById(R.id.new_prspcto_estado);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.estados_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        estado.setAdapter(arrayAdapter);

        mostrarL.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            lat = Double.parseDouble(extras.getString("latitud"));
            lon = Double.parseDouble(extras.getString("longitud"));
            tipot = extras.getString("tipotienda");
            Log.d("TIPO TIENDA", tipot);
            hidePicker(tipot);
        }
    }

    public void hidePicker(String tipot){
        switch (tipot){
            case "micro":
                imagePicker.setVisibility(View.GONE);
                break;
            case "comp":
                imagePicker.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void onClickMostrar(){
        mostrarL.setText("OCULTAR DATOS OPCIONALES");
        form1.setVisibility(View.VISIBLE);
        form2.setVisibility(View.VISIBLE);
        form3.setVisibility(View.VISIBLE);
        form4.setVisibility(View.VISIBLE);
        form5.setVisibility(View.VISIBLE);
        form6.setVisibility(View.VISIBLE);
        form7.setVisibility(View.VISIBLE);
        form8.setVisibility(View.VISIBLE);
        form9.setVisibility(View.VISIBLE);
        form10.setVisibility(View.VISIBLE);
        form11.setVisibility(View.VISIBLE);
        mostrarL.setId(R.id.btn_ocultar);
    }

    public void onClickOcultar(){
        mostrarL.setText("MOSTRAR DATOS OPCIONALES");
        form1.setVisibility(View.GONE);
        form2.setVisibility(View.GONE);
        form3.setVisibility(View.GONE);
        form4.setVisibility(View.GONE);
        form5.setVisibility(View.GONE);
        form6.setVisibility(View.GONE);
        form7.setVisibility(View.GONE);
        form8.setVisibility(View.GONE);
        form9.setVisibility(View.GONE);
        form10.setVisibility(View.GONE);
        form11.setVisibility(View.GONE);
        mostrarL.setId(R.id.btn_mostrar);
    }

    public void onClickImagePicker(View v){
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Selecciona imagen");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

        startActivityForResult(chooserIntent,PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data==null) {
                Log.d("tiene imagen", "no tiene imagen");
                return;
            }
            try {
                InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_mostrar:
                onClickMostrar();
                break;
            case R.id.btn_ocultar:
                onClickOcultar();
                break;
        }
    }
}
