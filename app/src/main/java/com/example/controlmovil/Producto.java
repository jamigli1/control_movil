package com.example.controlmovil;

public class Producto {

    private String marca, modelo, precio, precioR, serie, tipo;

    public Producto() {
    }

    public Producto(String marca, String modelo, String precio, String precioR, String serie, String tipo) {
        this.marca = marca;
        this.modelo = modelo;
        this.precio = precio;
        this.precioR = precioR;
        this.serie = serie;
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getPrecioR() {
        return precioR;
    }

    public void setPrecioR(String precioR) {
        this.precioR = precioR;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
