package com.example.controlmovil;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import java.util.Calendar;

public class CapturaDepActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText datePicker1, monto;
    private  EditText datePicker2, movimiento;
    private Spinner bancos;
    private Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_captura_dep);

        datePicker1  = (EditText) findViewById(R.id.datePicker1);
        datePicker2  = (EditText) findViewById(R.id.datePicker2);
        monto  = (EditText) findViewById(R.id.nw_dep_monto);
        movimiento = (EditText) findViewById(R.id.nw_dep_movimiento);

        datePicker1.setOnClickListener(this);
        datePicker2.setOnClickListener(this);

        btnSave = (Button) findViewById(R.id.nw_dep_btnsave);
        btnSave.setOnClickListener(this);

        bancos = (Spinner) findViewById(R.id.banco_spinner);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.bancos_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bancos.setAdapter(arrayAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.datePicker1:
                showDatePickerDialog(datePicker1);
                break;
            case R.id.datePicker2:
                showDatePickerDialog(datePicker2);
                break;
            case R.id.nw_dep_btnsave:
                saveDeposito();
                break;
        }
    }

    public void showDatePickerDialog(final EditText editText){
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final String selectedDate = dayOfMonth + "/" + (month+1) + "/" + year;
                editText.setText(selectedDate);
            }
        });

        newFragment.show(CapturaDepActivity.this.getSupportFragmentManager(),"datePicker");
    }

    public void saveDeposito(){
        nonEmpty((EditText) findViewById(R.id.nw_dep_monto));
        nonEmpty((EditText) findViewById(R.id.nw_dep_movimiento));
        nonEmpty((EditText) findViewById(R.id.datePicker1));
        nonEmpty((EditText) findViewById(R.id.datePicker2));

        if (nonEmpty((EditText) findViewById(R.id.nw_dep_monto)) && nonEmpty((EditText) findViewById(R.id.nw_dep_movimiento))
                && nonEmpty((EditText) findViewById(R.id.datePicker1)) && nonEmpty((EditText) findViewById(R.id.datePicker2))) {
            Log.d("Date1", datePicker1.getText().toString());
            Log.d("Date2", datePicker2.getText().toString());
            Log.d("Spinner", bancos.getSelectedItem().toString());
            Log.d("Monto", monto.getText().toString());
            Log.d("Movimiento", movimiento.getText().toString());
        }
    }

    public boolean nonEmpty(EditText editText){
        if (!TextUtils.isEmpty(editText.getText())){
            return true;
        } else {
            editText.setError("Error: Este campo es requerido.");
            return false;
        }
    }
}
