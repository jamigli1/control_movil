package com.example.controlmovil;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class SolicitudAdapter extends RecyclerView.Adapter<SolicitudAdapter.ViewHolder> {

    private Context context;
    private List<ClienteC> list;

    public SolicitudAdapter(Context context, List<ClienteC> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public SolicitudAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_solicitud,parent,false);
        return new SolicitudAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SolicitudAdapter.ViewHolder holder, int position){
        ClienteC clienteC = list.get(position);
        holder.txtUser.setText(clienteC.getNombre());
        holder.txtUser.setText(clienteC.getTipo());
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txtUser, txtNneg;

        public ViewHolder(View itemView) {
            super(itemView);
            txtUser = itemView.findViewById(R.id.solicitud_user);
            txtNneg = itemView.findViewById(R.id.solicitud_nneg);
        }
    }
}
