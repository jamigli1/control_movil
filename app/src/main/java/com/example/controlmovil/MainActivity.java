package com.example.controlmovil;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);

        if(settings.getString("usuario","default") != "default" && settings.getString("id","default") != "default") {
            Intent i = new Intent(this,MainMenu.class);
            startActivity(i);
            finish();
        }
    }

    public void onClickBtnLogin(View v) throws JSONException {

        EditText uname = (EditText) findViewById(R.id.uname_login);
        EditText pswd = (EditText) findViewById(R.id.psw_login);

        if(!TextUtils.isEmpty(uname.getText()) && !TextUtils.isEmpty(pswd.getText())){
            loginFunction(uname, pswd);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Por favor llena los campos correctamente");
            builder.setCancelable(true);
            builder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alerta = builder.create();
            alerta.show();
        }
    }

    private void loginFunction(EditText uname, final EditText pasw) throws JSONException {

        final String user = uname.getText().toString();
        final String pwd = pasw.getText().toString();

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.login_prgsbar);

        progressBar.setVisibility(ProgressBar.VISIBLE);

        RequestQueue queue = Volley.newRequestQueue(this);

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/login.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    String cad = response.replace("[","");
                    cad = cad.replace("]","");

                    JSONObject obj = new JSONObject(cad);

                    if(obj.getString("user").contains(user)){
                        progressBar.setVisibility(ProgressBar.GONE);
                        ejecuta(obj.getString("id"),obj.getString("user"),obj.getString("dis"),obj.getString("nombre"),obj.getString("tipo"));
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage("Verifique sus datos por favor.");
                        builder.setCancelable(true);
                        builder.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alerta = builder.create();
                        alerta.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("Error: ",error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method","iniciar");
                params.put("usuario",user);
                params.put("pass",pwd);
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void ejecuta(String id, String user, String dis, String agente, String tipo){
        Intent i = new Intent(this,MainMenu.class);

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        editor.putString("id",id);
        editor.putString("usuario",user);
        editor.putString("dis",dis);
        editor.putString("agente",agente);
        editor.putString("tipo",tipo);
        editor.commit();
        startActivity(i);
        this.finish();
    }

}
