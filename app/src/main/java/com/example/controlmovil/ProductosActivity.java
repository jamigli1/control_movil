package com.example.controlmovil;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ProductosActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;
    private EditText buscaEquipo;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private ProductoAdapter prueba;
    private List<Producto> pProductos;
    int positionClick = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);

        mList = (RecyclerView) findViewById(R.id.rView_Productos);
        pProductos = new ArrayList<>();
        mAdapter = new ProductoAdapter(getApplicationContext(),pProductos);
        prueba = new ProductoAdapter(getApplicationContext(),pProductos);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        //mList.setAdapter(mAdapter);
        mList.setAdapter(prueba);

        buscaEquipo = (EditText) findViewById(R.id.busca_equipos);

        buscaEquipo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        mList.addOnItemTouchListener(new ItemClickListener(getApplicationContext(), new ItemClickListener.OnItemClickListener() {
            @Override
            public void OnItemClick(View view, int position) {

                Log.d("Tamaño",String.valueOf(pProductos.size()));
                final TextView precio = (TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.prod_monto);
                final TextView precioR = (TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.prod_precioR);
                selectorMenu(precioR.getText().toString(),precio.getText().toString());
            }
        }));

        listarProductos();

    }

    private void filter(String text){
        List<Producto> temp = new ArrayList();
        for (Producto producto : pProductos){
            if (producto.getMarca().toLowerCase().contains(text.toLowerCase())){
                temp.add(producto);
            }
        }
        prueba.updateList(temp);
    }

    private void listarProductos() {

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String id_us = settings.getString("usuario","-1");

        Log.d("controlmovi - dis",dis);
        Log.d("controlmovi - usuario",id_us);


        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try{
                        JSONObject object = new JSONObject(response);
                        if(object.length() != 0){
                            generaListP(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getInvent");
                    params.put("codigo_dis",dis);
                    params.put("id_us",id_us);
                    params.put("op",String.valueOf(1));
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaListP(JSONObject jsonObject) throws JSONException {

        Iterator x = jsonObject.keys();
        JSONArray jsonArray = new JSONArray();

        String[] marca; String[] modelo; String[] serie;
        String[] precio; String[] precioR;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(jsonObject.get(key));
        }

        marca = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        modelo = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        serie = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");
        precio = (jsonArray.getString(3).replace("[","").replace("]","")).split(",");
        precioR = (jsonArray.getString(4).replace("[","").replace("]","")).split(",");

        for (int i=0; i<marca.length; i++){
            Producto producto = new Producto();
            producto.setMarca(marca[i].replace("\"",""));
            producto.setModelo(modelo[i].replace("\"",""));
            producto.setSerie(serie[i].replace("\"",""));
            producto.setPrecio(precio[i].replace("\"",""));
            producto.setPrecioR(precioR[i].replace("\"",""));
            Log.d("producto",producto.getModelo());
            pProductos.add(producto);
        }

    }

    public void onClickEquipos(View v){
        LinearLayout equipos = (LinearLayout) findViewById(R.id.linear_equipos);
        LinearLayout chips = (LinearLayout) findViewById(R.id.linear_chips);
        LinearLayout sEquipos = (LinearLayout) findViewById(R.id.search_equipos);
        LinearLayout sChips = (LinearLayout) findViewById(R.id.search_chips);
        Button btnSChips = (Button) findViewById(R.id.btn_busca_chips);

        equipos.setVisibility(View.VISIBLE);
        sEquipos.setVisibility(View.VISIBLE);
        chips.setVisibility(View.GONE);
        sChips.setVisibility(View.GONE);
        btnSChips.setVisibility(View.GONE);
    }

    public void onClickChips(View v){
        LinearLayout equipos = (LinearLayout) findViewById(R.id.linear_equipos);
        LinearLayout chips = (LinearLayout) findViewById(R.id.linear_chips);
        LinearLayout sEquipos = (LinearLayout) findViewById(R.id.search_equipos);
        LinearLayout sChips = (LinearLayout) findViewById(R.id.search_chips);
        Button btnSChips = (Button) findViewById(R.id.btn_busca_chips);

        equipos.setVisibility(View.GONE);
        sEquipos.setVisibility(View.GONE);
        chips.setVisibility(View.VISIBLE);
        sChips.setVisibility(View.VISIBLE);
        btnSChips.setVisibility(View.VISIBLE
        );
    }

    public void selectorMenu(String precioR, String precio){

        View mView = getLayoutInflater().inflate(R.layout.alert_select_producto,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        String nuevo = "Fija un precio entre $".concat(precioR).concat(" y ").concat(precio);

        Button btnopc1 = (Button) mView.findViewById(R.id.btn_nwProd_ok);
        Button btnopc2 = (Button) mView.findViewById(R.id.btn_nwProd_cancel);
        TextView textView = (TextView) mView.findViewById(R.id.nwprod_header);
        EditText editText = (EditText) mView.findViewById(R.id.new_prod_cant);

        textView.setText(nuevo);
        editText.setText(precioR);

        /*btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent1);
                dialog.dismiss();
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent2);
                dialog.dismiss();
            }
        });

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*/

        dialog.show();
    }
}
